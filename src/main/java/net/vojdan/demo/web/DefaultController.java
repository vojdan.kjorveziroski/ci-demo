package net.vojdan.demo.web;

import net.vojdan.demo.service.CommonUtilsService;
import org.joda.time.DateTime;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class DefaultController {

    private CommonUtilsService commonUtilsService;

    public DefaultController(CommonUtilsService commonUtilsService) {
        this.commonUtilsService = commonUtilsService;
    }

    @GetMapping("/")
    public String index(Model model) {
        DateTime currentDate = DateTime.now();
        boolean isFinkiBirthday = commonUtilsService.isFinkiBirthday(currentDate);
        if (isFinkiBirthday) {
            model.addAttribute("isFinkiBirthday", "ДА!");
        } else {
            model.addAttribute("isFinkiBirthday", "НЕ!");
        }
        return "index";
    }
}
