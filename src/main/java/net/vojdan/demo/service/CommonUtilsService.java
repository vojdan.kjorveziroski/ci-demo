package net.vojdan.demo.service;

import org.joda.time.DateTime;
import org.springframework.stereotype.Service;

@Service
public class CommonUtilsService {

    public boolean isFinkiBirthday(DateTime currentDate){
        return currentDate.dayOfMonth().get() == 30 && currentDate.monthOfYear().get() == 3;
    }
}
